<?php
namespace AdaLiszk\ArrayManipulation\Sorted\Tests\Unit;

use AdaLiszk\ArrayManipulation\Sorted\ArrayMerge;
use AdaLiszk\ArrayManipulation\Sorted\Tests\TestCase;
use ReflectionClass;

class ArrayMergeTest extends TestCase
{
    /**
     * Sanity check: The function/class is actually exists?
     *
     * @test
     */
    public function isClassExists()
    {
        $classReflection = new ReflectionClass(__ROOT_NAMESPACE__ . 'ArrayMerge');
        $this->assertTrue(true); // if there is no exception then it's exists
    }
    // --------------------------------------------------------------------------------------------

    /**
     * Sanity check: The function/class callable?
     *
     * @depends isClassExists
     * @test
     */
    public function isCallable()
    {
        $classReflection = new ReflectionClass(__ROOT_NAMESPACE__ . 'ArrayMerge');
        $array_merge = $classReflection->newInstance();

        $this->assertTrue(is_callable($array_merge));
    }
    // --------------------------------------------------------------------------------------------

    public function dummyInputDataProvider()
    {
        return [
            'Valid #1'      => [null, [[1],[1]]],
            'Valid #2'      => [null, [[1],[2]]],
            'Invalid #1'    => ['TypeError', ['a','b']],
            'Invalid #2'    => ['TypeError', [(object)[1], (object)[2]]]
        ];
    }

    /**
     * Sanity check: The input has to be an array of arrays
     *
     * @param null|string $exceptionName if there is an expected exception
     * @param array $arguments of the callable class/function
     *
     * @dataProvider dummyInputDataProvider
     * @depends isCallable
     * @test
     */
    public function isOnlyAcceptArrayOfArrays(?string $exceptionName, array $arguments)
    {
        $classReflection = new ReflectionClass(__ROOT_NAMESPACE__ . 'ArrayMerge');
        /** @var ArrayMerge $array_merge */
        $array_merge = $classReflection->newInstance();

        if (!empty($exceptionName)) $this->expectException($exceptionName);

        $output = $array_merge(...$arguments);

        $this->assertTrue(true); // we don't need to do anything
    }
    // --------------------------------------------------------------------------------------------

    public function inputDataProvider()
    {
        return [
            '[1] + [2]' => [
                [ [1],[2] ],
                [1,2]
            ],
            '[2] + [1]' => [
                [ [2],[1] ],
                [1,2]
            ],
            '[1,2] + [3,4]' => [
                [ [1,2],[3,4] ],
                [1,2,3,4]
            ],
            '[3,4] + [1,2]' => [
                [ [3,4],[1,2] ],
                [1,2,3,4]
            ],
            '[1,2,3,4] + [5,6,7,8]' => [
                [ [1,2,3,4],[5,6,7,8] ],
                [1,2,3,4,5,6,7,8]
            ],
            '[5,6,7,8] + [1,2,3,4]' => [
                [ [5,6,7,8],[1,2,3,4] ],
                [1,2,3,4,5,6,7,8]
            ],
            '[1,3,5,7] + [2,4,6,8]' => [
                [ [1,3,5,7],[2,4,6,8] ],
                [1,2,3,4,5,6,7,8]
            ],
            '[2,4,6,8] + [1,3,5,7]' => [
                [ [2,4,6,8],[1,3,5,7] ],
                [1,2,3,4,5,6,7,8]
            ],
            '[1,2,3] + [4,5,6,7,8,9]' => [
                [ [1,2,3],[4,5,6,7,8,9] ],
                [1,2,3,4,5,6,7,8,9]
            ],
            '[1,2,3,4,5,6] + [7,8,9]' => [
                [ [1,2,3,4,5,6],[7,8,9] ],
                [1,2,3,4,5,6,7,8,9]
            ],
            '[1,3,5] + [2,4,6,7,8,9]' => [
                [ [1,3,5],[2,4,6,7,8,9] ],
                [1,2,3,4,5,6,7,8,9]
            ],
            '[1,2,3,4,6,8] + [5,7,9]' => [
                [ [1,2,3,4,6,8],[5,7,9] ],
                [1,2,3,4,5,6,7,8,9]
            ],
            '[1,3] + [2,4] + [5,7] + [6,8] + [9]' => [
                [ [1,3],[2,4],[5,7],[6,8],[9] ],
                [1,2,3,4,5,6,7,8,9]
            ],
        ];
    }

    /**
     * Sanity check: The input has to be an array of arrays
     *
     * @param array $arguments of the callable class/function
     * @param array $result expected output for the call
     *
     * @dataProvider inputDataProvider
     * @depends isOnlyAcceptArrayOfArrays
     * @test
     */
    public function itHasTheExpectedResult(array $arguments, array $result)
    {
        $classReflection = new ReflectionClass(__ROOT_NAMESPACE__ . 'ArrayMerge');
        /** @var ArrayMerge $array_merge */
        $array_merge = $classReflection->newInstance();

        $output = [];
        foreach ($array_merge(...$arguments) as $item)
            $output[] = $item;

        $this->assertSame($result, $output);
    }
    // --------------------------------------------------------------------------------------------
}
